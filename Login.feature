Feature: Login

    @positive_case
    Scenario: TC.Log.002.001-The user has successfully logged in with valid information
        Given The user is already on the login page
        When Enter the page https://secondhand.binaracademy.org/
        And Click the Sign in button
        And Fill in the login form with valid information, such as a registered email address and password
        And Click the Login button
        Then The user has successfully logged in to his account and is redirected to the main page

    @negative_case
    Scenario: TC.Log.002.002-The user has successfully logged in using an email address in capital letters
        Given The user is already on the login page
        When Enter the page https://secondhand.binaracademy.org/
        And Cligit ck the Sign in button
        And Fill in the login form with valid information, such as an email address in all capital letters and a registered password
        And Click the Login button
        Then The user has successfully logged in to his account and is redirected to the main page

